# GIT
Intern: Nguyen Ngo Lap

### 1. What is Git? 
Version Control System or Source Code Manager. Main purpose is to manage source code.

- Distributed version control:
    - No single master repo, many different repositories
    - Tracks changes, not versions. The matter is whether change sets are applied or not
- Open source, free, fast and reliable
- History of Git: SCCS (1972), RSC (1982), CVS (1986 - 1990), SVN (2000), BitKeeper SCM (2000 - important)


### 2. Installing Git
\- Configuration:

1. System:
    - File: etc\gitconfig
    - `git config --system`
2. User:
    - File: [~/%user%]\.gitconfig
    - `git config --global`
3. Project:
    - File: my_project/.git/config
    - `git config`

\- Useful command: `git help <command>` or `man git-<command>` (opening the manual)

### 3. Getting started
\- Basic flow:

1. Initialize: `git init`
2. Make changes
3. Add the changes: `git add`
4. Commit the changes: `git commit`

\- Commit message: Should be descriptive and well-labelled

\- Tracking commits: `git log`

### 4. Git concepts and architecture
\- Three-trees architecture:

- repo
- staging index
- working

\- Git use checksum to check changes. SHA-1 is used to create checksum (40 chars) <- Commit ID (important)

\- HEAD:

- Pointer to "tip" of current branch
- Last state of repo (last checked out)
- Pointer to parent of next commit (where writing commits takes place)

### 5. Making changes to files
\- `git status` - report the difference among the trees

- Statuses: Untracked, modified, new file added, deleted, ...

\- `git diff` - show the changes of the modified files (between working tree and staging index)

\- `git diff --staged` - (like above) (between staging index and the repo)

\- `git rm <file name>` - remove the file from the project

\- `git checkout` - discard file changes in the working directory

\- **Note:** 

- Manually delete then git rm, the file will be sent to Trashcan
- Use git rm directly will permanently delete the file
- Rename will be treated as delete + add new file. After adding to staging index, Git will compare and consider it as rename if the data is similar (50%?)
- Rename and move is the same in Git
- `git mv` will automatically add changes to the staging index


### 6. Using Git with a real project

`git diff --color-words` - Comparison mode: highlight the different words side by side. Default mode: highlight the different lines separately

`git commit -am "<comment>"` - add everything and commit. However, untracked and deleted files not included. More suitable for modification

### 7. Undoing changes
\- Undo changes in working directory: `git checkout`

- Update files in the working trees to match with files from the repo or switch to the specified branch

\- To stay in the same branch: `git checkout -- <file>`

\- Unstaging files (undoing changes to staging index): `git reset HEAD <file>`

\- Undoing commits: `git commit --amend -m"<comment>"` (only the last commit)

\- Retrieving old versions: `git checkout <SHA of the old commit> -- <the file needs fetching>`

\- Reverting a commit: `git revert <SHA reference>`

\- Using reset to undo commits:

- Soft: `git reset --soft <SHA reference>`
    - just move the HEAD (no change to staging index or working dir)
- Mixed (default): `git reset --mixed <SHA reference>`
    - move the HEAD
    - changes staging index to match repo
    - no change to working directory
- Hard: `git reset --hard`
    - change staging index and working directory to match with repo (all commit obliterated)

\- Removing untracked files:`git clean` (permanently deleted)

- Option:
    - -n: Test run
    - -f: Forced run

### 8. Ignoring files
\- Using .gitignore files:

- Basic expressions: * ? [aeiou] [0-9]
- Negate expressions with !
- Ignore the whole directory: Aaa/Bbb/
- \# comment, blank lines are skipped

\- What to ignore:

- compiled source code
- packages and compressed files
- logs and databases
- operating system generated files
- user-uploaded assets (images, PDFs, videos)

\- Should see:

- https://help.github.com/articles/ignoring-files/
- https://github.com/github/gitignore

\- User-specific ignore: `git config --global core.excludesfile <.gitignore file location>`

\- Ignoring tracked files: `git rm --cached <file>`

\- Tracking empty directories:

- Git keeps track of files, not directories
- Solution: Put a little file in the directory. .gitkeep often used

### 9. Navigating the commit tree
\- **tree-ish**: A reference to the commit. Something that points at a commit
\- Ways to reference:

- Full SHA-1 hash
- Short SHA-1 hash
    - at least 4 characters
    - unambiguous (8-10 or even more, dependent on the size of the project)
- HEAD pointer
    - Branch reference, tag reference
- Ancestry
    - Using caret: HEAD/SHA hash/master + ^/^^/^^^/... (the number of caret = the number of commits to trace back). E.g. master^ (parent), acf87504^^ (grandparent)...
    - Using the tilde: HEAD~ + the number of commits to trace back. E.g. HEAD~3

\- Terms when listing:

- tree: Directory
- blob: File

\- Getting more from git log:

- Should see: https://git-scm.com/docs/git-log
- Useful options:
    - `--oneline` - oneline list
    - `-<number>` - limit the number of shown commit 
    - `since/until/before/...="<time>"` - E.g. "2017-08-03", "2 days/weeks ago", 2.weeks, ...
    - `--grep="<string>"` - Regular expression search  
    - `<SHA>..<SHA>` - List range
    - `<SHA>..<SHA> <file>` - Commit relevant to the file in that range
    - `-p` - Patch. Show a diff of the changes in each commit
    - `--format=<format, e.g. oneline/short/medium/full>`

\- Viewing commits:

- Command: git show <SHA>
- Can also view blob, tree, tag

\- Comparing commit:

- Command: git diff <SHA>..<SHA>

### 10. Branching
Branch are cheap, low cost, easy to work with, allows trying new ideas, ect.

\- Creating new branch: `git branch <new branch name>`

\- Switching branch: `git checkout <branch>`

\- Creating and switching at the same time: `git checkout -b <new branch name>`

- Branch must be mostly clean (untracked files are fine) before switching. In case of uncommited changes:
    - Checkout the original file
    - Commit the changes
    - Stash

\- Comparing branch: `git diff <branch>..<branch>`
- Caret can also be used to compare with a branch's ancestor

\- Showing branches included in current branch: `git branch --merged`

\- Renaming branch: `git branch -m/--move <old name> <new_name>`

\- Deleting branch: `git branch -d/--delete <branch to delete>`

- Checks:
    - Cannot delete current branch
    - Cannot delete branch that has changes that aren't merged
    - Force delete: `git branch -D <branch to delete>`

\- Configuturing the command prompt:

- Configuring Prompt String 1: `export PS1="<new PS1>"`
- Configuring PS1 to show current working folder and branch: `PS1='\W $(__git_ps1 "(%s)")'`

### 11. Merging branches
\- Merging branches: `git merge <branch to merge with>`

- Options:
    - --no-ff: No fast-forward merging (no silent merging)
    - --ff-only: Only fast-forward merging
- Fast-forward merging: Current HEAD pointer is included in the branch-to-merge's tree. No new commit is made, the missing commits will just be moved to the main branch

\- **Conflicts:** Conflict in the same line of code in different commits, ect.

\- Resolving merge conflict:

- Abort merge: git merge --abort
- Resolve manually
- Use a merge tool

\- Strategies to reduce merge conflicts:

- Keep lines short
- Keep commits small and focused
- Beware of stray edits to whitespace (spaces, tabs, line returns)
- Merge often
- Track changes to master: Keep sync with master branch

### 12. Stashing changes

- Store changes temporarily without having to commit them to the repo.
- A special fourth area in Git
- Work similarly to commit, but without SHA

\- Saving changes into the stash: `git stash save "<message>"`

\- Viewing list changes in the stash: `git stash list`

\- Viewing a change in the stash: `git stash show [/p] <stash item>`

\- Retrieving changes from stash:

- `git stash pop <stash item>` - Pull the change and remove it from the stash
- `git stash apply <stash item>` - Pull the change and leave a copy in the stash
- If <stash item> not specified, it will be treated as stash@{0}
- In case of merging conflicts, stash item will be preserved even if pop is used

\- Delete stash items: `git stash drop <stash item>`

\- Delete everything in the stash: `git stash clear`

### 13. Remotes
\- **origin/master:** A branch on local machine that references the remote server branch

\- **push:** Put the commits from the computer upto the remote server

- `git push <remote> <branch>`

\- **fetch:** Fetch the changes other people push upto the remote server into the local origin/master branch

- `git fetch <remote alias>`
- Guidelines:
    - Fetch before working
    - Fetch before pushing
    - Fetch often

\- **pull:** Fetch then merge

- `git pull <remote> <branch>`

\- **Flow:** commit some changes -> fetch from the server to keep the originm/master branch in sync -> merge -> push up again

\- Adding a remote repo: `git remote add <alias> <url>`

\- Showing remote list: `git remote [-v]`

\- Removing remote: `git remote rm <remote alias>`

\- Creating a remote branch: `git push -u <remote> <branch>`

\- Cloning a remote repo: `git clone "link" [folder name]`

\- Tracking remote branches:

- Help Git know which to push to and which to pull from without argument
- Push with the -u option to setup tracking while pushing

\- Checking out remote branches:

- Setup local branch to track remote branch: `git branch <new local tracking branch> <remote branch>`

\- Pushing to an updated remote branch:

- Fetch the update, merge and push again
Deleting a remote branch:
- `git push <remote> :<remote branch>`
or
`git push <remote> --delete <remote branch>`


### 14. Tools and next steps
\- Setting up alias:

- Edit the config file directly, or
`git config --global alias.<shortkey> "<command>"`

\- Password caching: https://help.github.com/articles/caching-your-github-password-in-git/

\- Git GUI reference: https://git.wiki.kernel.org/index.php/Interfaces,_frontends,_and_tools
